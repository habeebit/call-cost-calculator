import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CheapestOperatorComponent } from './cheapest-operator.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

describe('CheapestOperatorComponent', () => {
  let component: CheapestOperatorComponent;
  let fixture: ComponentFixture<CheapestOperatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheapestOperatorComponent ],
      imports: [
        HttpClientModule,
        FormsModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheapestOperatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
