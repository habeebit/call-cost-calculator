import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { OperatorsService } from '../shared/operators.service';
import { CheapestOperatorService } from './cheapest-operator.service';
import { PrefixCostOperator } from '../shared/operator.model';

@Component({
  selector: 'app-cheapest-operator',
  templateUrl: './cheapest-operator.component.html',
  styleUrls: [
    './cheapest-operator.component.css',
  ],
  providers: [
    OperatorsService,
    CheapestOperatorService,
  ]
})

export class CheapestOperatorComponent implements OnInit {
  formSubmitted = false;
  operatorCosts: Array<PrefixCostOperator>;
  operatorFound: boolean;
  cheapestOperator: {
    name: string,
    cost: number,
  };

  constructor(private opertatorsService: OperatorsService, private cheapestOperatorService: CheapestOperatorService) { }

  ngOnInit() {
    this.opertatorsService
      .getOperatorsData()
      .subscribe(data => this.operatorCosts = this.cheapestOperatorService.prepareData(data));
  }

  onSubmit(form: NgForm) {
    this.formSubmitted = true;
    this.operatorFound = false;
    const cheapestOperator = this.cheapestOperatorService.getCheapestOperator(form.value.phoneNumber, this.operatorCosts);

    if (cheapestOperator !== null) {
      this.operatorFound = true;
      this.cheapestOperator = {
        name: cheapestOperator.operator,
        cost: cheapestOperator.cost,
      };
    }
  }
}
