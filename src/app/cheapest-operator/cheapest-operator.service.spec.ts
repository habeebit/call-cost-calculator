import { TestBed } from '@angular/core/testing';

import { CheapestOperatorService } from './cheapest-operator.service';

describe('CheapestOperatorService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CheapestOperatorService = TestBed.get(CheapestOperatorService);
    expect(service).toBeTruthy();
  });

  it('should prepare data and make it ready for subsequent calls', () => {
    const operatorData = {
      A: [{ prefix: '1', cost: 0.9 }],
      B: [{ prefix: '1', cost: 0.92 }]
    };
    const result = [
      { prefix: '1', cost: 0.9, operator: 'A' },
      { prefix: '1', cost: 0.92, operator: 'B' }
    ];
    const service: CheapestOperatorService = TestBed.get(CheapestOperatorService);
    expect(service.prepareData(operatorData)).toEqual(result);
  });

  it('should retrun cheapest operator if prefix avaialable', () => {
    const operatorCosts = [
      { prefix: '1', cost: 0.9, operator: 'A' },
      { prefix: '1', cost: 0.92, operator: 'B' }
    ];
    const phoneNumber = '11235678654';
    const result = { prefix: '1', cost: 0.9, operator: 'A' };
    const service: CheapestOperatorService = TestBed.get(CheapestOperatorService);
    expect(service.getCheapestOperator(phoneNumber, operatorCosts)).toEqual(result);
  });

  it('should retrun null if prefix avaialable', () => {
    const operatorCosts = [
      { prefix: '1', cost: 0.9, operator: 'A' },
      { prefix: '1', cost: 0.92, operator: 'B' }
    ];
    const phoneNumber = '51235678654';
    const result = null;
    const service: CheapestOperatorService = TestBed.get(CheapestOperatorService);
    expect(service.getCheapestOperator(phoneNumber, operatorCosts)).toEqual(result);
  });
});
