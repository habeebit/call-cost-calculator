import { Injectable } from '@angular/core';
import { Operators, PrefixCostOperator } from '../shared/operator.model';

@Injectable({
  providedIn: 'root'
})

export class CheapestOperatorService {

  constructor() { }

  prepareData(operatorData: Operators) {
    const operatorCosts = [];

    for (const operator of Object.keys(operatorData)) {
      for (const rates of Object.keys(operatorData[operator])) {
        operatorCosts.push({...operatorData[operator][rates], operator});
      }
    }

    return operatorCosts;
  }

  getCheapestOperator(phoneNumber: string, operatorCosts: Array<PrefixCostOperator>) {
    for (let i = 0; i < phoneNumber.length; i++) {
      const phoneNumberPrefix = phoneNumber.substr(0, phoneNumber.length - i);
      const operatorCost = operatorCosts.find(data => data.prefix === phoneNumberPrefix);

      if (operatorCost !== undefined) {
        return operatorCost;
      }
    }

    return null;
  }
}
