interface PrefixCost {
    prefix: string;
    cost: number;
}

export interface Operators {
    [operator: string]: Array<PrefixCost>;
}

export interface PrefixCostOperator {
    prefix: string;
    cost: number;
    operator: string;
}
