import { TestBed } from '@angular/core/testing';

import { OperatorsService } from './operators.service';
import { HttpClientModule } from '@angular/common/http';

describe('OperatorsService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientModule]
  }));

  it('should be created', () => {
    const service: OperatorsService = TestBed.get(OperatorsService);
    expect(service).toBeTruthy();
  });
});
