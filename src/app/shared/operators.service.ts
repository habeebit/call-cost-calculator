import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Operators } from './operator.model';

@Injectable({
  providedIn: 'root'
})

export class OperatorsService {

  constructor(private httpClient: HttpClient) { }

  getOperatorsData() {
    return this.httpClient
      .get<Operators>('assets/data/operator-costs.json');
  }
}
